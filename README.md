## Follow clean architecture [Get your hands dirty with clean architecture](https://github.com/greyfairer/clean-architecture-example)


- adapters
	- mes-courses-persistence
	- mes-courses-web


- mes-courses-application
	- application
		- port
			- in
			- out
		- service
	- domain

- mes-courses-configuration

- mes-courses-testdata

- common
