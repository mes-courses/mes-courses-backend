package fr.sare.mescoursesbackend.application.rest;

import fr.sare.mescoursesbackend.domain.entities.Product;
import fr.sare.mescoursesbackend.domain.use_cases.GetAllProducts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sareaboudousamadou.
 */
@RestController
public class ProductAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(ProductAdapter.class);
    private final GetAllProducts getAllProducts;

    public ProductAdapter(GetAllProducts getAllProducts) {
        this.getAllProducts = getAllProducts;
    }

    @GetMapping(value = "/api/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductApi>> getAllProducts() {
        LOG.info("Fetching all products");
        List<ProductApi> result = getAllProducts.execute()
                .stream().map(this::toProductApi)
                .collect(Collectors.toList());

        return ResponseEntity.ok(result);
    }

    private ProductApi toProductApi(Product product) {
        ProductApi result = new ProductApi();
        result.setId(product.id());
        result.setName(product.name());
        result.setPrice(product.price());

        return result;
    }
}
