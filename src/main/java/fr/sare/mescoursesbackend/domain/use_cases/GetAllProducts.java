package fr.sare.mescoursesbackend.domain.use_cases;

import fr.sare.mescoursesbackend.domain.entities.Product;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author sareaboudousamadou.
 */
@Component
public class GetAllProducts {
    private final ProductPort productPort;

    public GetAllProducts(ProductPort productPort) {
        this.productPort = productPort;
    }

    public List<Product> execute() {
        return productPort.getAllProducts();
    }
}
