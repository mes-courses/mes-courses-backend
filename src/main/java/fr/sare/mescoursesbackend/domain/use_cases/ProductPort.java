package fr.sare.mescoursesbackend.domain.use_cases;

import fr.sare.mescoursesbackend.domain.entities.Product;

import java.util.List;
import java.util.Optional;

/**
 * @author sareaboudousamadou.
 */
public interface ProductPort {
    List<Product> getAllProducts();
}
