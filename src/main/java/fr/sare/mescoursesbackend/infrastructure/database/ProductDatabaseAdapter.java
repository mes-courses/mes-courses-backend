package fr.sare.mescoursesbackend.infrastructure.database;

import fr.sare.mescoursesbackend.domain.entities.Product;
import fr.sare.mescoursesbackend.domain.use_cases.ProductPort;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author sareaboudousamadou.
 * // TODO use mapstruct for conversion
 */
@Component
public class ProductDatabaseAdapter implements ProductPort {

    private final ProductJpaRepository productJpaRepository;

    public ProductDatabaseAdapter(ProductJpaRepository repository) {
        productJpaRepository = repository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productJpaRepository.findAll().stream()
                .map(this::toProduct)
                .collect(Collectors.toList());
    }

    private Product toProduct(ProductJpa productJpa) {
        return Optional.ofNullable(productJpa)
                .map(product -> new Product(product.getId(), product.getName(), product.getPrice()))
                .orElseThrow(() -> new NullPointerException("ProductJpa should not be null"));
    }
}
