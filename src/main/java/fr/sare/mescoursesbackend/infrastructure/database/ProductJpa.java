package fr.sare.mescoursesbackend.infrastructure.database;

import fr.sare.mescoursesbackend.domain.entities.Product;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * @author sareaboudousamadou.
 */
@Entity
@Table(name = "t_product")
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ProductJpa {

    @Id
    @Column(name = "pro_id")
    private UUID id;

    @Column(name = "pro_name")
    private String name;

    @Column(name = "pro_price", precision = 16, scale = 2)
    private BigDecimal price;
}
