package fr.sare.mescoursesbackend.infrastructure.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author sareaboudousamadou.
 * // TODO add pageable
 */
@Repository
public interface ProductJpaRepository extends JpaRepository<ProductJpa, UUID> {
}
