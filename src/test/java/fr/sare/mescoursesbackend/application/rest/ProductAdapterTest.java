package fr.sare.mescoursesbackend.application.rest;

import fr.sare.mescoursesbackend.domain.entities.Product;
import fr.sare.mescoursesbackend.domain.use_cases.GetAllProducts;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


/**
 * @author sareaboudousamadou.
 * // TODO ASA add approval test
 */

@WebMvcTest(controllers = ProductAdapter.class)
class ProductAdapterTest {

    private static final String APPLE = "Apple";

    @MockBean
    private GetAllProducts getAllProducts;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void shouldGetAllProductWhenGetEndpointIsCalled() throws Exception {
        final UUID appleId = UUID.randomUUID();
        Product product = new Product(appleId, APPLE, new BigDecimal("10"));
        List<Product> products = List.of(product);
        BDDMockito.given(getAllProducts.execute()).willReturn(products);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/products")
                .secure(true)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", Is.is(product.id().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Is.is(product.name())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].price", Is.is(product.price().intValue())));
    }

}