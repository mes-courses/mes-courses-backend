package fr.sare.mescoursesbackend.domain.use_cases;

import fr.sare.mescoursesbackend.domain.entities.Product;
import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

import static org.mockito.BDDMockito.given;

/**
 * @author sareaboudousamadou.
 * TODO add plugin for code coverage and pitest
 */
@ExtendWith(MockitoExtension.class)
class GetAllProductsTest {
    private static final String APPLE = "Apple";
    private static final String TEA = "Tea";
    private static final String WATER = "Water";

    @Mock
    private ProductPort productPort;
    private GetAllProducts userStorie;

    @BeforeEach
    void setUp() {
        userStorie = new GetAllProducts(productPort);
    }

    @Test
    void shouldReturnAllProducts() {
        final UUID appleId = UUID.randomUUID();
        final UUID teaId = UUID.randomUUID();
        final UUID waterId = UUID.randomUUID();
        given(productPort.getAllProducts()).willReturn(List.of(
                new Product(appleId, APPLE, BigDecimal.ONE),
                new Product(teaId, TEA, BigDecimal.TEN),
                new Product(waterId, WATER, BigDecimal.ZERO)
        ));

        List<Product> products = userStorie.execute();

        Assertions.assertThat(products)
                .extracting("id", "name", "price")
                .contains(Tuple.tuple(appleId, APPLE, BigDecimal.ONE),
                        Tuple.tuple(teaId, TEA, BigDecimal.TEN),
                        Tuple.tuple(waterId, WATER, BigDecimal.ZERO));
    }
}
