package fr.sare.mescoursesbackend.infrastructure.database;

import fr.sare.mescoursesbackend.domain.entities.Product;
import org.assertj.core.api.Assertions;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sareaboudousamadou.
 */
@DataJpaTest
@TestPropertySource(properties = {"spring.jpa.hibernate.ddl-auto=validate"})
class ProductDatabaseAdapterTest {

    private ProductDatabaseAdapter productDatabaseAdapter;
    @Autowired
    private ProductJpaRepository productJpaRepository;

    @BeforeEach
    void setUp() {
        productDatabaseAdapter = new ProductDatabaseAdapter(productJpaRepository);
    }

    @Test
    void shoudlInjectComponent() {
        assertNotNull(productJpaRepository);
    }

    @Test
    @Sql("classpath:addProducts.sql")
    void getAllProducts() {
       List<Product> products = productDatabaseAdapter.getAllProducts();

        Assertions.assertThat(products)
                .extracting( "name", "price")
                .contains(Tuple.tuple( "Apple", new BigDecimal("12.00")),
                        Tuple.tuple( "Tea", new BigDecimal("120.02")),
                        Tuple.tuple("Rice", new BigDecimal("19.00")));
    }
}